import functools
from pprint import pprint
import requests
from math import ceil
from flask import (
    Blueprint, current_app,flash, g, redirect, render_template, request, session, url_for
)

#from .solr import select,query,get_solr
from solr.client import select,query,get_solr


bp = Blueprint('search', __name__)

@bp.route('/', methods=('GET', 'POST'))
def search():
    if request.method == 'GET':
        #r = select(querystring = request.query_string)
        q = request.args['q']
        r = select(params = dict(request.args))
    return render_template('search/search.html',result = r,q=q)


@bp.route('/query', methods=('GET', 'POST'))
def request_query():
    if request.method == 'GET':
        q = request.args['q']
        page = int(request.args.get('page',1))
        pages = 0
        start = current_app.config['ROWS_PER_PAGE'] * (page-1)
        request_body = {"query":q}
        request_body['limit'] = current_app.config['ROWS_PER_PAGE']
        request_body['offset'] = start
        request_body['facet'] = current_app.config['SOLR_FACETS']  
                
        #filters
        filter=[]
        for a in request.args:
            if a[:3] == 'fq_':
                filter.append(request.args[a])        

        if len(filter):
            request_body['filter']=filter

        r = query(request_body)

        if 'response' in r:
            pages = ceil(r['response']['numFound']/current_app.config['ROWS_PER_PAGE'])
    
    return render_template('search/results.html',result = r,q=q, page=page, pages=pages, start=start,  current_url=request.url)


@bp.route('/suggest', methods=('GET', 'POST'))
def suggest():
    if request.method == 'GET':
        solr = get_solr()
        q = request.args['q']
        querystring = 'suggest=true&suggest.dictionary=mySuggester&suggest.q='+q
        solr = "/".join([solr,'suggest'])
        r = requests.get(solr,querystring) 
        r = r.json()
        s = r['suggest']['mySuggester']
        return s


@bp.route('/detail/<id>', methods=('GET', 'POST'))
def detail(id):
    if request.method == 'GET':
        r = select(params = {'q':'id:'+id})
    return render_template('search/detail.html',result = r)
