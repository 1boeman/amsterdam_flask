from flask import Blueprint,redirect,url_for, request, render_template, current_app,flash,send_from_directory
from flask_babel import _
from flask_login import login_required
from werkzeug.exceptions import abort
from werkzeug.utils import secure_filename
from amsterdam import db
from amsterdam.models import Item,Type
from amsterdam.forms import ItemForm,UploadForm
import os
import datetime
from pprint import pprint


item = Blueprint('item', __name__)

@item.route('/<alias>')
def item_view(alias):
    #item = Item.query.filter_by(alias=alias).join(Item.cities).join(Item.types).first()
    item = Item.query.filter_by(alias = '/'+alias).first()

    if not item:
        abort(404, "{0}  bestaat niet (meer).".format(alias))
    
    return render_template('item/item.html',item = item,img_server=current_app.config['IMAGE_SERVER'])


@item.route('/list/<type_name>/<page>')
@item.route('/list/<type_name>')
def item_type(type_name,page=0):
    _type = Type.query.filter_by(name = type_name).first()
    items = Item.query.join(Item.types).filter_by(id=_type.id).all()
    title = type_name
    return render_template('item/list.html',items = items, title=title)

#https://github.com/cptpugwash/basic-crud-flask/blob/master/app/views.py

@item.route('/item/add',methods=('GET', 'POST'))
#@login_required
def add():
    form = ItemForm()
    if form.validate():
        data = {}
        item = Item( author_id=1)

        form.populate_obj(item)
        db.session.add(item)
        db.session.commit()
    return render_template('item/edit.html',  form=form)


@item.route('/item/edit/<alias>',methods=('GET', 'POST'))
#@login_required
def update(alias):
    item = Item.query.filter_by(alias = '/'+alias).first()
    form = ItemForm(obj=item)
    if form.validate():
        form.populate_obj(item)
        db.session.commit()
    return render_template('item/edit.html',  form=form,alias=alias)


@item.route('/uploadform',methods=('GET', 'POST'))
#@login_required
def uploadform():
    form = UploadForm() 
    upload_path = get_upload_path()
    files = []
    dirname=''
    if os.path.isdir(upload_path):
        dirname = get_upload_path(True)
        files = os.listdir(upload_path)
    
    return render_template('item/edit_upload.html',  form=form,files=files,dirname=dirname)


@item.route('/upfile_view/<subdir>/<filename>')
def upload_view(subdir,filename):
    path = os.path.join(current_app.config['UPLOAD_PATH'],subdir)
    return send_from_directory(path,filename)


@item.route('/upfile_dir')
@item.route('/upfile_dir/<dirname>')
#@login_required
def upload_directory_view(dirname = None):
    if not dirname:
        directory = current_app.config['UPLOAD_PATH']
    else:
        directory = os.path.join(current_app.config['UPLOAD_PATH'],dirname)
    files = os.listdir(directory)
    return render_template('item/upload_index.html', files=files, dirname = dirname)


def get_upload_path(dirname_only=False):
    now = datetime.datetime.now()
    year = now.strftime("%Y")
    month = now.strftime("%m")
    dirname = '-'.join([year,month])
    if dirname_only:
        return dirname
    else:
        upload_path = os.path.join(current_app.config['UPLOAD_PATH'] , dirname)
    return upload_path


@item.route('/up_file',methods=('GET', 'POST'))
#@login_required 
def upload_file():
    form = UploadForm() 
    if form.validate_on_submit():
        f = form.file.data
        upload_path = get_upload_path()
        if not os.path.isdir(upload_path):
            os.makedirs(upload_path)

        filename = secure_filename(f.filename)
        f.save(os.path.join(upload_path,filename))
        flash(_('File was uploaded successfully'))
        return redirect(url_for('item.uploadform'))


