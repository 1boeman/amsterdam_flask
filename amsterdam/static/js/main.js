(function(){
    // helpers
    function s(selector){
        return document.querySelectorAll(selector);
    }
 
    function mkClick(selectorOrEl,callback){
        var listen = function(el,callback){
            el.addEventListener('click',function(e){
                console.log(this)
        
                e.preventDefault();
                e.stopPropagation();
                if (typeof callback == 'function'){
                    callback.apply(this);             
                } else if (typeof clickHandlers[this.dataset.handler] == 'function'){
                    clickHandlers[this.dataset.handler].apply(this);
                }
            });
        }
        if (typeof selectorOrEl == 'string'){
            s(selectorOrEl).forEach(el => {
               listen(el,callback);
            });
        } else {
            listen(selectorOrEl,callback);
        }
    }


    function getJSON (url,callback){
        var request = new XMLHttpRequest();
        request.open('GET', url, true);

        request.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status < 400) {
                  // Success!
                    var data = JSON.parse(this.responseText);
                    callback(data);
                } else {
                  // Error :(
                }
            }
        };

        request.send();
        request = null;
    }


    class SearchResults{
        activeFilters = [];
        q = '';
        constructor(){
            let urlParams = new URLSearchParams(window.location.search).entries(); 
            this.parseParams(urlParams)
        }

        parseParams(urlParams){
            for (let [name,value] of urlParams){
                if (name == 'q'){
                    this.q = value; 
                } else if(name.substring(0,3) == 'fq_'){
                    this.activeFilters.push(value)
                }
            }
        }         
        
        search(){
            var newLoc =''
            var base = window.location.href.split('search/query')[0]
            newLoc = base + 'search/query?q='+this.q
            for (let i =0; i < this.activeFilters.length; i++){
                newLoc += '&fq_' + i + '='+this.activeFilters[i];
            }           
            window.location.href = newLoc;
        }

        removeFilter (filter){
            var index = this.activeFilters.indexOf(filter);
            console.log(index)
            if (index !== -1) {
                this.activeFilters.splice(index, 1);
            }        
        }
        
        addFilter(filter){
            var index = this.activeFilters.indexOf(filter);
            if (index == -1) {
                this.activeFilters.push(filter)
            }
        }

        displayFilters(facetClassName,activeFilterContainerClassName){
            var facetItems = s(facetClassName);
            var cntnr = s(activeFilterContainerClassName)[0];
            var html = '';
            var self_ = this;
            facetItems.forEach(function(facetItem){
                let f = facetItem.dataset.filtervalue
                if (self_.activeFilters.indexOf(f) !=-1){
                    console.log(f)
                   facetItem.classList.add('nodisplay');
                   html += '<div><span class="tag is-success activeFilter" data-filter='+escape(f)+'>'
                   html += f.split(':')[1];
                   html += ' <button class="delete"></button></span></div>';

                } else {
                    mkClick(facetItem,function(){
                        self_.addFilter(this.dataset.filtervalue); 
                        self_.search()
                    });
                }
            });


            if (html.length){
                cntnr.innerHTML = '<h4>Filters:</h4>' + html; 
                mkClick('.activeFilter',function(){
                    self_.removeFilter(unescape(this.dataset.filter)); 
                    self_.search()
                }); 
            }
        }
    }
 

    // event handler functions 
    var bodyClassHandlers = {
        "page-search-query":function(){
            const sr = new SearchResults();
            sr.displayFilters('.facet-list-item','.activeFilters');
            s('.filters')[0].classList.add('show');
            s('.filters')[0].classList.remove('hide');
        }
        
        
    };


    var clickHandlers = {
       "home_search_submit":function(){
           s('#home_search')[0].submit();
        }

    };


    // event-handlers setup
    if (typeof bodyClassHandlers[document.body.id] == 'function'){
        bodyClassHandlers[document.body.id]();
    }

    mkClick('.clickme');
   
    var l = new autoComplete({
        selector: '#autoComplete',
        onSelect:function(e,term,item){
              s('#home_search')[0].submit();
        },
        source: function(term, response){
            getJSON('/search/suggest?q=' + term, function(data){ 
                var suggest_array = [];
                for (let key in data){
                    if (data[key]['numFound'] > 0){
                        for (let suggestion of data[key]['suggestions']){
                            suggest_array.push(suggestion['term'])
                        } 

                    }
                }
                response(suggest_array); 
            });
        }
    });
})();
