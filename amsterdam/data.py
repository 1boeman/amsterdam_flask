import click
from flask import current_app, g
from flask.cli import with_appcontext
import requests
from werkzeug.security import check_password_hash, generate_password_hash 
from . import db, create_app, models
from .solr import get_solr


def init_app(app):
    app.cli.add_command(init_db_command)
    app.cli.add_command(import_db_command)
    app.cli.add_command(index_solr_command)



@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    db.create_all() 
    generate_super_user()
    generate_base_types()
    click.echo('Initialized the database.')


@click.command('import-db')
@click.argument('domain')
@with_appcontext
def import_db_command(domain):
    """import data from drupal"""
    import_db(domain,'location')
    import_db(domain,'area')
    click.echo('imported the data.')


@click.command('index-solr')
@with_appcontext
def index_solr_command():
    index_solr()
    click.echo('Indexed data in solr')


def index_solr():
    for item in db.session.query(models.Item).all():
        created = item.created.isoformat()
        if 'Z' not in created:
            created = created + 'Z'
        
        updated = ''
        if item.updated:
            updated = item.updated.isoformat()
            if 'Z' not in updated:
                updated = updated + 'Z'    
        
        data = {
            "id":item.id,
            "title":item.title,
            "author_name":item.author.name,
            "author_id":item.author_id,
            "created":created,
            "updated":updated,
            "lang":item.lang,
            "body":item.body,
            "alias":item.alias,
            "image":item.image,
            "cities":[],
            "addresses":[],
            "streets":[],
            "types":[],
        }        
        
        for city in item.cities:
            data['cities'].append(city.name)
        
        for address in item.addresses:
            address_string = ' '.join([address.street,address.street_number,address.zip or '' ,address.city.name])
            street_string = ', '.join([address.street,address.city.name])
            data['addresses'].append(address_string)
            data['streets'].append(street_string)

        for _type in item.types:
            data['types'].append(_type.name)

        index_solr_doc(data)
        

def index_solr_doc(data):

    #print (data)
    solr = get_solr()
    solr = "/".join([solr,"update"])
    command = {"add":{"doc":data},"commit":{}}
    r = requests.post(solr,json=command )
    #print (r)
    return r


def import_db(domain,entity_type):
    print ('import')
    finished = False
    i = 0 
    while not finished:
        offset = i*10
        i = i+1
        response = requests.get(domain + '/jsonapi/node/' + entity_type + '?page[limit]=10&page[offset]=' + str(offset))
        data = response.json()
        if 'data' not in data or not len(data['data']):
            finished = True
            print ('no more data')
        else:
            process_data(data,entity_type)            
 

def generate_super_user():
    user = models.User( name = "joriso", email = "jorisojoriso@gmail.com" )
    user.set_password('changethis')
    db.session.add(user)
    db.session.commit()
 

def generate_base_types():
    location = models.Type(name = 'location')
    area = models.Type(name = 'area')
    for t in [area,location]:
        db.session.add(t)
    db.session.commit()


def get_image(url):
    print (url)
    response = requests.get(url)
    data = response.json()
    path = data['data'][0]['attributes']['uri']['url']
    return path


def process_data(data,entity_type):
    for l in data['data']:

        # get image_path
        image = l['relationships']['field_image']
        image_api = image['links']['related']['href'] 
        image_path = get_image(image_api)    
                 
        # insert item
        a = l['attributes']
        item = models.Item( author_id=1, title = a["title"], body = a['body']['value'], alias = a['path']['alias'], image=image_path)
        db.session.add(item)
        db.session.commit()

        type_names = a['field_type']
        for type_name in type_names:
            type_ = models.Type.query.filter_by(name = type_name).first()
            if not type_:
                type_ = models.Type(name = type_name)
                db.session.add(type_)
                db.session.commit()
            item.types.append(type_)
            db.session.commit()

        city_name = a['field_city']
        city = models.City.query.filter_by(name = city_name).first()
        if not city:
            city = models.City(name = city_name)
            db.session.add(city)
            db.session.commit()

        if entity_type == 'location':
            print( 'location')
            type_ = models.Type.query.filter_by(name="location").first()
            item.types.append(type_)
            address = models.Address.query.filter_by( street = a["field_street"], street_number = a['field_street_number']).first()
            if not address:
                address = models.Address(street = a['field_street'], street_number = a['field_street_number'], zip = a['field_zip'], city_id = city.id)
            db.session.add(address)
            db.session.commit()
            item.addresses.append(address)
            item.cities.append(city)
 
        elif entity_type == 'area':
            print ('area')
            type_ = models.Type.query.filter_by(name="area").first()
            item.types.append(type_)
            item.cities.append(city)
                     
        db.session.commit()
       
        links = a['field_link']
        for ln in links:
            link = models.Link(item_id = item.id, url = ln['uri'],text = ln['title'])
        db.session.add(link)
        db.session.commit()
        
