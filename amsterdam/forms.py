from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField,TextAreaField,SelectField
from flask_wtf.file import FileField
from wtforms.validators import DataRequired
from amsterdam.models import Item,Type,City,Address
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from amsterdam import db


def type_choices():      
    return db.session.query(Type)


def city_choices():      
    return db.session.query(City)


def address_choices():
    return db.session.query(Address)


def address_labels(address):
    c = address.city
    l = str(address.id) + '] ' + address.street + ' ' + address.street_number + ' ' + c.name
    return l

class ItemForm(FlaskForm):

    alias = StringField('Alias', validators=[DataRequired()])
    title = StringField('Title', validators=[DataRequired()])
    image = StringField('Image', validators=[DataRequired()])
    lang =  SelectField('lang', choices=[('en','English'),('nl','Dutch')]) 
    types = QuerySelectMultipleField(u'Type', get_label='name',     
                               validators=[DataRequired()],
                               query_factory=type_choices)

    cities = QuerySelectMultipleField(u'City', get_label='name',     
                               validators=[],
                               query_factory=city_choices)

    addresses = QuerySelectMultipleField(u'Address', get_label=address_labels,     
                               validators=[],
                               query_factory=address_choices)


    body = TextAreaField('Body', validators=[DataRequired()])
    submit = SubmitField('Save')


class UploadForm(FlaskForm):
    file = FileField('File')
    submit = SubmitField('Submit')


