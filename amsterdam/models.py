from flask_login import UserMixin
from sqlalchemy.sql import func
from werkzeug.security import generate_password_hash, check_password_hash
from . import db


class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True) 
    email = db.Column(db.String(100), unique=True,nullable=False)
    password = db.Column(db.String(100),nullable=False)
    name = db.Column(db.String(200),nullable=False)
    items = db.relationship("Item", back_populates="author")

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)


# for locations
address_item = db.Table('address_item',
   db.Column('address_id', db.Integer, db.ForeignKey('address.id'), primary_key=True),
   db.Column('item_id', db.Integer,db.ForeignKey('item.id'),primary_key=True,index=True),
) 


# for areas (no address - directly link to cities)
city_item = db.Table('city_item',
   db.Column('city_id', db.Integer, db.ForeignKey('city.id'), primary_key=True),
   db.Column('item_id', db.Integer,db.ForeignKey('item.id'),primary_key=True,index=True),
) 


type_item = db.Table('type_item',
   db.Column('type_id', db.Integer, db.ForeignKey('type.id'), primary_key=True),
   db.Column('item_id', db.Integer,db.ForeignKey('item.id'),primary_key=True,index=True),
) 


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer,db.ForeignKey('user.id'),nullable=False)
    author = db.relationship("User", back_populates = "items")
    created = db.Column(db.DateTime(), server_default=func.now())
    updated = db.Column(db.DateTime(), onupdate=func.now())
    public = 
    title = db.Column(db.String(1000), nullable=False)
    body = db.Column(db.Text(),nullable=False)
    alias = db.Column(db.String(1000),unique=True)
    image = db.Column(db.String(1000))
    lang = db.Column(db.String(4), nullable=False, server_default='en')
    translation_of = db.Column(db.Integer,db.ForeignKey('item.id'))
    addresses = db.relationship("Address",secondary = address_item )
    cities = db.relationship("City", secondary = city_item )
    types = db.relationship("Type",secondary = type_item )
    db.Index('ix_item_author', author_id)
    db.Index('ix_item_alias', alias)
    

class Type (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable = False)


# for items that represent periods or events
class Period (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    item_id = db.Column(db.Integer,db.ForeignKey('item.id'),nullable=False)
    start_date = db.Column(db.DateTime(), nullable=False) 
    end_date = db.Column(db.DateTime(), nullable=True)   
    sub_title = db.Column(db.String(1000), nullable=True)


class City (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable = False)
    addresses = db.relationship ("Address",back_populates="city")


class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated = db.Column(db.DateTime(timezone=True), onupdate=func.now())
    street = db.Column(db.String(1000), nullable = False)
    street_number = db.Column(db.String(100),nullable=False)
    city_id = db.Column(db.Integer,db.ForeignKey('city.id'),nullable=False)
    city = db.relationship("City", back_populates="addresses")
    zip = db.Column(db.String(100))
    db.Index('ix_address_street_city', street, street_number, city_id, unique=True)
    db.Index('ix_address_city',city_id)


class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    item_id = db.Column(db.Integer,db.ForeignKey('item.id'),nullable=False)
    url = db.Column(db.String(2000),nullable = False)
    text = db.Column(db.Text,nullable=False)
    db.Index ('ix_link_item_id',item_id)


