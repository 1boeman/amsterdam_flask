import os
from flask_login import LoginManager
from flask import Flask, request, g, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from flask_babel import Babel, _
from werkzeug.exceptions import abort

db = SQLAlchemy()
def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        WTF_CSRF_ENABLED=False,
        SECRET_KEY='dev-development',
        SQLALCHEMY_DATABASE_URI = 'sqlite:///' +  os.path.join(app.instance_path ,'amsterdam.sqlite'),
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
        IMAGE_SERVER="http://192.168.178.84:5000",
        MAX_CONTENT_LENGTH= 8192 * 1024,
        UPLOAD_EXTENSIONS = ['.jpg','jpeg', '.png', '.gif'],
        UPLOAD_PATH = os.path.join(app.instance_path,'files'),
        UPLOAD_PUBLIC = os.path.join(app.instance_path,'files'),
 
        LANGUAGES = ['nl', 'en'],
        BABEL_DEFAULT_LOCALE = 'en',
        SOLR='http://localhost:8983/solr',
        SOLR_API= 'http://localhost:8983/api',
        SOLRCORE='amsterdam',
        ROWS_PER_PAGE=10,
        SOLR_FACETS = {
            "types":{
                "type": "terms",
                "field": "types",
            },
            "streets":{
                "type": "terms",
                "field": "streets",
            },
            "cities":{
                "type": "terms",
                "field": "cities",
            }
        })

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    
    babel = Babel(app)
    
    db.init_app(app)

    from . import data 
    data.init_app(app)    

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from . import solr
    solr.init_app(app)

    from . import search
    app.register_blueprint(search.bp,url_prefix='/search')
    app.register_blueprint(search.bp,url_prefix='/<lang_code>/search')


    from .item import item as item_blueprint
    app.register_blueprint(item_blueprint)
    app.register_blueprint(item_blueprint,url_prefix='/<lang_code>')


    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass


    # BOF babel     
    @babel.localeselector
    def get_locale():
        return g.get('lang_code', app.config['BABEL_DEFAULT_LOCALE'])


    @app.url_defaults
    def add_language_code(endpoint, values):
        if 'lang_code' in values or not g.lang_code:
            return
        if app.url_map.is_endpoint_expecting(endpoint, 'lang_code'):
            values['lang_code'] = g.lang_code


    @app.before_request
    def ensure_lang_support():
        lang_code = g.get('lang_code', None)
        if lang_code and lang_code not in app.config['LANGUAGES']:
            return abort(404)


    @app.url_value_preprocessor
    def get_lang_code(endpoint, values):
        if values is not None:
            g.lang_code = values.pop('lang_code',app.config['BABEL_DEFAULT_LOCALE'])

    # EOF babel     
 

    @app.context_processor
    def menu(): 
        other_languages = app.config['LANGUAGES'].copy()
        other_languages.remove(g.lang_code) 
        m = []

        for l in other_languages:
            m.append({'text':l, 'url':'/'+l})

        return dict(menu=m)
 #           {'text':_('shops'),'url':'/'},
 #           {'url':'/', 'text':_('out')},
 #           {'text':_('Home'),'url':'/'},
  #      ])
            

    @app.context_processor
    def body_class():
        if request.path == "/":
            body_class_name = 'page-home'
        else:
            body_class_name = str(request.path).replace("/"+g.lang_code,"").replace("/","-")
            body_class_name = "page" + body_class_name
        return dict(body_class_name=body_class_name)


    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return request.path


    # directory structure from Drupal
    @app.route('/sites/<path:filename>')
    def custom_static(filename):
        return send_from_directory('static/sites', filename)


    return app


