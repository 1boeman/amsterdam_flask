from setuptools import setup

setup(
    name='amsterdam',
    packages=['amsterdam','forms'],
    include_package_data=True,
    install_requires=[
        'flask','wtforms'
    ],
)


