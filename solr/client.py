import json
import requests
import click
from flask import current_app, g



def select(querystring = b'', params = {} ): 
    solr = get_solr() 
    solr = "/".join([solr,'select']) 
    if len(querystring):  
        r = requests.get(solr,querystring) 
    elif params: 
        r = requests.get(solr,params)  
 
    return r.json() 

    
#https://lucene.apache.org/solr/guide/8_6/json-request-api.html
def query(data):
    solr = get_solr() 
    solr = "/".join([solr,'query']) + '?q.op=AND'
    r = requests.post(solr,json=data)
    return r.json()      



def get_solr ():
    if 'solr' not in g:
        g.solr = "/".join([current_app.config['SOLR'],current_app.config['SOLRCORE']])
    return g.solr


